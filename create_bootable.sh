#!/bin/bash

### Config ###
ROOT_PASSWORD="rehdd"

if [ "$EUID" -ne 0 ]; then
  echo "This script must be run as root"
  exit 1
fi

### Cleanup ###
printf "\nCleanup\n"
rm -rf reHDD-Bootable.iso
rm -rf out/

### Preconditions ###
printf "\nPreconditions\n"
current_dir=$(pwd)
cd ./root/root/reHDD/
git stash
git checkout master
git pull
git submodule init
git submodule update

cd $current_dir

cd ./root/root/reHDDPrinter/
git stash
git checkout main
git pull
cd $current_dir

### Build ###
printf "\nBuild\n"
START=$(date +%s.%N)
mkdir -p out
kiwi-ng system build --description ./ --set-repo obs://openSUSE:Tumbleweed/standard --target-dir ./out
mv -f ./out/*.iso reHDD_Bootable.iso 2>/dev/null; true

chmod -R 777 ./out
chmod 777 reHDD_Bootable.iso

DURATION=$(echo "$(date +%s.%N) - $START" | bc)
echo "Time taken: $DURATION seconds"