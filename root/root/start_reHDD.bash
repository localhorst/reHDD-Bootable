#!/bin/bash

echo "##########################################################################################################"
echo "##########################################################################################################"                                                                                                        
                                                                                                         
echo "                                        HHHHHHHHH     HHHHHHHHHDDDDDDDDDDDDD        DDDDDDDDDDDDD"        
echo "                                        H:::::::H     H:::::::HD::::::::::::DDD     D::::::::::::DDD"     
echo "                                        H:::::::H     H:::::::HD:::::::::::::::DD   D:::::::::::::::DD"   
echo "                                        HH::::::H     H::::::HHDDD:::::DDDDD:::::D  DDD:::::DDDDD:::::D"  
echo "rrrrr   rrrrrrrrr       eeeeeeeeeeee      H:::::H     H:::::H    D:::::D    D:::::D   D:::::D    D:::::D" 
echo "r::::rrr:::::::::r    ee::::::::::::ee    H:::::H     H:::::H    D:::::D     D:::::D  D:::::D     D:::::D"
echo "r:::::::::::::::::r  e::::::eeeee:::::ee  H::::::HHHHH::::::H    D:::::D     D:::::D  D:::::D     D:::::D"
echo "rr::::::rrrrr::::::re::::::e     e:::::e  H:::::::::::::::::H    D:::::D     D:::::D  D:::::D     D:::::D"
echo " r:::::r     r:::::re:::::::eeeee::::::e  H:::::::::::::::::H    D:::::D     D:::::D  D:::::D     D:::::D"
echo " r:::::r     rrrrrrre:::::::::::::::::e   H::::::HHHHH::::::H    D:::::D     D:::::D  D:::::D     D:::::D"
echo " r:::::r            e::::::eeeeeeeeeee    H:::::H     H:::::H    D:::::D     D:::::D  D:::::D     D:::::D"
echo " r:::::r            e:::::::e             H:::::H     H:::::H    D:::::D    D:::::D   D:::::D    D:::::D" 
echo " r:::::r            e::::::::e          HH::::::H     H::::::HHDDD:::::DDDDD:::::D  DDD:::::DDDDD:::::D"  
echo " r:::::r             e::::::::eeeeeeee  H:::::::H     H:::::::HD:::::::::::::::DD   D:::::::::::::::DD"   
echo " r:::::r              ee:::::::::::::e  H:::::::H     H:::::::HD::::::::::::DDD     D::::::::::::DDD"     
echo " rrrrrrr                eeeeeeeeeeeeee  HHHHHHHHH     HHHHHHHHHDDDDDDDDDDDDD        DDDDDDDDDDDDD"        
echo " "     
echo "                                reHDD - hard drive refurbishing tool"
echo "                                Available under GPL 3.0"
echo "                                https://git.mosad.xyz/localhorst/reHDD"
echo " "                   
echo "##########################################################################################################"
echo "##########################################################################################################"


cd /root/reHDD

make clean

make release

clear

./reHDD


