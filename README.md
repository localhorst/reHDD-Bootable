# reHDD-Bootable

Scripts and tools to create a bootable image with reHDD based on openSuse. 

## Requirements

The scripts are used to run within a openSuse-based machine as host. 

```
git submodule init
git submodule update
```

sudo zypper addrepo http://download.opensuse.org/repositories/Virtualization:/Appliances:/Builder/openSUSE_Tumbleweed kiwi-appliance-builder

sudo zypper --gpg-auto-import-keys install python3-kiwi xorriso

## Build Image

`sudo bash ./create_bootable.sh`

## Test 
### BIOS
`qemu-system-x86_64 -boot d -cdrom reHDD_Bootable.iso -m 2048 -smp 2`
### UEFI
`qemu-system-x86_64 -bios /usr/share/qemu/ovmf-x86_64.bin -boot d -cdrom reHDD_Bootable.iso -m 2048 -smp 2`
